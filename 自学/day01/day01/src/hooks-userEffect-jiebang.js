import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
// 学习React Hooks 时，我们要改掉生命周期函数的概念（人往往有先入为主的毛病，所以很难改掉），因为Hooks叫它副作用，所以componentWillUnmount也可以理解成解绑副作用。这里为了演示用useEffect来实现类似componentWillUnmount效果，先安装React-Router路由,进入项目根本录，使用npm进行安装。


// 相当于一个组件
function Index() {
  useEffect(() => {
    console.log("index页面")
  })
  return <h1>index.com</h1>
}
function List() {
  useEffect(() => {
    console.log("List页面")
  })
  return <h1>List.com</h1>
}

function HooksUserEffectJiebang() {
  const [count, setCount] = useState(0)

  return (
    <div>
      <p>you clicked {count} times</p>
      <button onClick={() => { setCount(count + 1) }}>click me</button>
      <Router>
        <ul>
          <li><Link to="/">首页</Link></li>
          <li><Link to="/list/">列表</Link></li>

        </ul>
        <Route path="/" exact component={Index} />
        <Route path="/list/" component={List} />
      </Router>

    </div>
  )

}
export default HooksUserEffectJiebang;