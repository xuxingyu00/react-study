import React, { Component, Fragment } from 'react'
// 引入css文件
import "./Tianjiashijian.css"
import Chajian from './Chajian';
import Jsx from './Jsx'
import HooksUserEffectJiebang from './hooks-userEffect-jiebang'

class Tianjiashijian extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputValue: "学习了什么内容",
            list: ['学习建立react项目', '学习如何显示组件', '学习如何添加事件', '学习如何遍历数组', '学习如何删除数组元素', '学习点击聚焦', '学习如何让标签解析带html标签的数据','组件的拆分','组件传值(子传父）']
        }
        this.deleteItem=this.deleteItem.bind(this)
    }
    render() {
        return (
            <Fragment>
                {/* 注释的正确写法    js中类名要用clasName而不是class*/}
                <div>
                    <label htmlFor="shurukuang">增加服务</label>
                    <input id="shurukuang" className="shurukuang" value={this.state.inputValue} onChange={this.inputChange.bind(this)} />
                    <button onClick={this.dianji.bind(this)}>增加学习内容</button>
                </div>
                {/*  <ul>
                    {this.state.list.map((item, index) => {
                        //    key不加的话会报警告
                        return (
                            <li
                                key={index + item}
                                // 传值的时候记得是以,分割
                                onClick={this.deleteItem.bind(this, index)}
                                // 这种写法可以解析输入框中的html写法
                                dangerouslySetInnerHTML={{ __html: item }}
                            >

                            </li>
                        )
                    })}
                </ul>
                <input value="这就是一个输入框" />
              */}
               <ul>
                 {
                     this.state.list.map((item,index)=>{
                         return (
                            <Chajian
                            key={index+item}
                            content={item}
                            index={index}
                            deleteItem={this.deleteItem}
        
                        />
                         )
                     })
                 }
             </ul>
             <HooksUserEffectJiebang></HooksUserEffectJiebang>
             <br />
              <Jsx /> 
            </Fragment>

            
            
        )
    }


    // 监听输入框的内容变化
    inputChange(e) {

        this.setState({
            inputValue: e.target.value
        })
    }
    //点击添加事件
    dianji() {
        if (this.state.inputValue !== "") {
            this.setState({
                list: [...this.state.list, this.state.inputValue],
                inputValue: ''
            })
        } else {
            alert('请输入添加的要学习的内容')
        }

        console.log(this.state.list);
    }
    // 点击删除
    deleteItem(index) {
        console.log(index)
        let list = this.state.list

        list.splice(index, 1)

        this.setState({
            list: list
        })
    }

}

export default Tianjiashijian
